// HENIKA

import * as THREE from 'three';
import Stats from 'three/addons/libs/stats.module.js';


const mainRenderer = new THREE.WebGLRenderer({ powerPreference: "high-performance" });
document.body.appendChild(mainRenderer.domElement);
mainRenderer.domElement.style.imageRendering = "pixelated";
const mainScene = new THREE.Scene();
const mainCamera = new THREE.OrthographicCamera(window.innerWidth / -2, window.innerWidth / 2, window.innerHeight / 2, window.innerHeight / -2, 0.01, 500);
const gameContainer = new THREE.Object3D();
gameContainer.position.set(0, 13, -1);
const UIContainer = new THREE.Object3D();
mainScene.add(gameContainer, UIContainer);
//mainScene.position.y = 13;
let oldWindow = document.body.clientWidth / document.body.clientHeight;
onWindowResize();
function onWindowResize() {
	if (document.body.clientHeight / document.body.clientWidth < 2) {
	  mainScene.scale.set(document.body.clientHeight / 720, document.body.clientHeight / 720, 1);
 	
		mainRenderer.setPixelRatio(720 / document.body.clientHeight);
		mainScene.position.x = 0.37
		mainScene.position.y = 0.1
	} else {
		mainScene.scale.set(document.body.clientWidth / 360, document.body.clientWidth / 360, 1);
	}
	
//	alert(mainScene.scale.x)
	
	mainRenderer.setSize(document.body.clientWidth, document.body.clientHeight);
  

	mainCamera.left = -document.body.clientWidth / 2;
	mainCamera.right = document.body.clientWidth / 2;
	mainCamera.top = document.body.clientHeight / 2;
	mainCamera.bottom = -document.body.clientHeight / 2;
	
	
	
	mainCamera.updateProjectionMatrix;
  oldWindow = document.body.clientWidth / document.body.clientHeight;
}





function visibilityChange() {
  let hidden = 'hidden'
  if (hidden in document)
    document.addEventListener('visibilitychange', onchange)
  else if ((hidden = 'mozHidden') in document)
    document.addEventListener('mozvisibilitychange', onchange)
  else if ((hidden = 'webkitHidden') in document)
    document.addEventListener('webkitvisibilitychange', onchange)
  else if ((hidden = 'msHidden') in document)
    document.addEventListener('msvisibilitychange', onchange)
  else if ('onfocusin' in document)
    document.onfocusin = document.onfocusout = onchange
  else
    window.onpageshow = window.onpagehide = window.onfocus = window.onblur = onchange
  function onchange(evt) {
    let v = false,
      h = true,
      evtMap = {
        focus: v,
        focusin: v,
        pageshow: v,
        blur: h,
        focusout: h,
        pagehide: h
      }
    evt = evt || window.event
    let windowHidden = false
    if (evt.type in evtMap) {
      windowHidden = evtMap[evt.type]
    } else {
      windowHidden = this[hidden]
    }
      if (windowHidden) {
         Howler.mute(true)
      } else {
         Howler.mute(false)
      }
  }
  if (document[hidden] !== undefined)
    onchange({
      type: document[hidden] ? 'blur' : 'focus'
    })
}


function loadPIC(url) {
	return new Promise(resolve => {
		new THREE.TextureLoader().load(url, resolve)
	})
}

const tex = [];
let loadingCount = 20;
for (let i = 0; i < 21; i++) {
	loadPIC(`textures/tex${i}.png`).then(texture => {
		tex[i] = texture;
		tex[i].colorSpace = THREE.SRGBColorSpace;
		tex[i].minFilter = THREE.NearestFilter;
		tex[i].magFilter = THREE.NearestFilter;
		loadingCount ? loadingCount-- : createGraphics();
	});
}




document.body.addEventListener('touchstart', onDocumentTouchStart, false);
document.body.addEventListener('touchend', onDocumentTouchEnd, false);
document.body.addEventListener('touchmove', onDocumentTouchMove, false);


const transparentBasicMaterial = new THREE.MeshBasicMaterial({ color: 0xFFFFFF, transparent: true, opacity: 0.0000001 })
//const bubbleBodyMaterial = new THREE.MeshBasicMaterial({ color: 0xFFFFFF, transparent: true, opacity: 0.0000001, side: THREE.DoubleSide })

const whiteBasicMaterial = new THREE.MeshBasicMaterial({ color: 0xFFFFFF })


let isCellsFieldActive = false;
let activeTower = -1;
let cellsToCheck = [];
let newCells = [];
const cell = [];
const bubblesContainer = new THREE.Object3D();
const bubbleLine = [];
const shootingBall = [];
const shootingBallPic = [
	[],
	[9, 10, 11]
];
const shootingTowerUI = new THREE.Object3D();
const shootingTowerPic = [
	[],
	[],
	[12, 13, 14],
	[15, 16, 17],
	[18, 19, 20]
]
const playingFieldTopUIContainer = new THREE.Object3D();
const playingFieldBottomUIContainer = new THREE.Object3D();
let currentLevel = 0;
let cellsTargets = [];
let bubblesTargets = [];
const aimRay = new THREE.Raycaster();

let targetBubbles = [];
let shotPath = [];

let bubblesWaveCount = 0;

let autoWave = false;


function createGraphics() {
	
	
	  
  for (let i = 0; i < 54; i++) {
  	cell[i] = new THREE.Object3D();
  	
  	
  	cell[i].content = -1;
  	cell[i].tempMark = 0;
  	
  	
  	
  	cell[i].traceData = -1;
  	cell[i].pathData = false;
  	
  	
  	cell[i].clickableArea = new THREE.Mesh(new THREE.PlaneGeometry(38, 38), transparentBasicMaterial);
    cell[i].clickableArea.visible = false;
  
  	cell[i].pic = new THREE.Mesh(new THREE.PlaneGeometry(46, 48), new THREE.MeshBasicMaterial({ map: tex[0], transparent: true }));
    cell[i].pic.geometry.translate(0, 5, 0);
    cell[i].add(cell[i].pic, cell[i].clickableArea);
    cell[i].position.set(-152 + 38 * (i % 9), -260 + 38 * Math.floor(i / 9), -40);
    
    cell[i].animation = `idle`;
    cell[i].add(cell[i].pic);
    gameContainer.add(cell[i]);
    
    
  }
  
  
  
  
  
  
  
  shootingTowerUI.outerCircle = new THREE.Mesh(new THREE.PlaneGeometry(230, 230), new THREE.MeshBasicMaterial({ map: tex[12], transparent: true }));
  shootingTowerUI.aim = new THREE.Mesh(new THREE.PlaneGeometry(120, 120), new THREE.MeshBasicMaterial({ map: tex[18], transparent: true }));
  shootingTowerUI.aim.visible = false;
  shootingTowerUI.aimLine = new THREE.Mesh(new THREE.PlaneGeometry(16, 630), new THREE.MeshBasicMaterial({ map: tex[15], transparent: true }));
  shootingTowerUI.aimLine.geometry.translate(0, 315, 0);
  shootingTowerUI.aimLine.geometry.rotateZ(-Math.PI * 0.5);
  shootingTowerUI.aimLine.visible = false;
  
 // 

  
  shootingTowerUI.add(shootingTowerUI.aimLine, shootingTowerUI.outerCircle);
    
  
  
  
  
  shootingTowerUI.visible = false;
  
  
  
  
  
  
  gameContainer.add(shootingTowerUI, bubblesContainer);
  
  
  
  
  
  bubblesContainer.position.set(0, 277, -2);
  
  bubblesContainer.add(shootingTowerUI.aim)
  
    
  playingFieldTopUIContainer.plane = new THREE.Mesh(new THREE.PlaneGeometry(360, 80), new THREE.MeshBasicMaterial({ color: 0x222222 }));
  playingFieldTopUIContainer.add(playingFieldTopUIContainer.plane);
  
  playingFieldTopUIContainer.position.set(0, 320, -1);
  
  
  playingFieldBottomUIContainer.plane = new THREE.Mesh(new THREE.PlaneGeometry(360, 93), new THREE.MeshBasicMaterial({ color: 0x222222 }));
  playingFieldBottomUIContainer.add(playingFieldBottomUIContainer.plane);
  
  playingFieldBottomUIContainer.position.set(0, -313.5, -1);
  
  mainScene.add(playingFieldTopUIContainer, playingFieldBottomUIContainer);
  
  createCampaignLevel(0)
}

const campaignLevelData = [];

campaignLevelData[0] = {
	mana: 0,
	colors: 3,
	
	bubblesMode: "regularWave",
	
	bubblesMatrix: [
		[-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[-1, 0, 0, 1, 1, 0, 2, 2, 0, 0, -1],
		[-1, -1, 1, 1, 0, 0, 0, 2, -1, -1],
		[0, 1, 1, 0, 0, 2, 2, 2, 2, 0, 0],
		[0, 0, 1, 2, 2, 2, 2, 2, 0, 0],
		[0, 0, 0, 0, 2, 2, 2, 0, 0, 0, 0],
		[0, 0, 1, 0, 2, 2, 0, 0, 1, 1],
		[0, 1, 0, 0, 2, 0, 0, 1, 1, 1, 1],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
	  [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
	]
	
};


function createCampaignLevel(chosenCampaignLevel) {
	if (campaignLevelData[chosenCampaignLevel].mana > 0) {
		
	}
	
  for (let i = 0; i < 54; i++) {
		if (cell[i].content == -1) {
			cell[i].movable = 1;
  		cell[i].content = Math.floor(Math.random() * campaignLevelData[chosenCampaignLevel].colors);
	  	for (let j = 0; j < campaignLevelData[chosenCampaignLevel].colors; j++) {
	  	  if (checkMatches([i], false, false)) {
	  	  	cell[i].content = (cell[i].content + 1) % campaignLevelData[chosenCampaignLevel].colors;
	    	} else {
	    		break;
	    	}
	  	}
	    cell[i].pic.material.map = tex[cell[i].content];
		}
	}
	
	if (campaignLevelData[chosenCampaignLevel].bubblesMode == "regularWave") {
		
		moveBubblesWave();
	}
	
}


let rearBubblesLine = 100;
let frontBubblesLine = 100;
function moveBubblesWave() {
	if (bubblesWaveCount < campaignLevelData[currentLevel].bubblesMatrix.length) {
    
    bubbleLine[rearBubblesLine] = new THREE.Object3D();
    bubbleLine[rearBubblesLine].bubble = [];
    for (let j = 0; j < 11 - (rearBubblesLine % 2); j++) {
    	bubbleLine[rearBubblesLine].bubble[j] = new THREE.Object3D();
    	bubbleLine[rearBubblesLine].bubble[j].pic = new THREE.Mesh(new THREE.PlaneGeometry(38, 36), transparentBasicMaterial);
    	bubbleLine[rearBubblesLine].bubble[j].pic.geometry.translate(0, 1, 0);
    	bubbleLine[rearBubblesLine].bubble[j].body = new THREE.Mesh(new THREE.CylinderGeometry(20, 20, 200, 8, 1, true), transparentBasicMaterial);
    	bubbleLine[rearBubblesLine].bubble[j].body.rotation.x = Math.PI * 0.5;
    	bubbleLine[rearBubblesLine].bubble[j].body.visible = false;
    	if (campaignLevelData[currentLevel].bubblesMatrix[rearBubblesLine - 100][j] >= 0) {
    		// bubbleLine[bubbleLine.length - 1].bubble[bubbleLine[bubbleLine.length - 1].bubble.length - 1].content = campaignLevelData[currentLevel].bubblesMatrix[rearBubblesLine - 100][j];
    		bubbleLine[rearBubblesLine].bubble[j].content = Math.floor(Math.random() * 3);
    
    		bubbleLine[rearBubblesLine].bubble[j].pic.material = new THREE.MeshBasicMaterial({ map: tex[6 + bubbleLine[rearBubblesLine].bubble[j].content], transparent: true })
    	} else {
    		bubbleLine[rearBubblesLine].bubble[j].pic.visible = false;
    		bubbleLine[rearBubblesLine].bubble[j].content = -1;
    		bubbleLine[rearBubblesLine].bubble[j].pic.scale.set(0, 0, 1);
    
    		bubbleLine[rearBubblesLine].bubble[j].body.scale.set(2, 2, 2);
    
    	}
    	bubbleLine[rearBubblesLine].bubble[j].add(bubbleLine[rearBubblesLine].bubble[j].pic, bubbleLine[rearBubblesLine].bubble[j].body);
    	bubbleLine[rearBubblesLine].bubble[j].position.set(-(10 - (rearBubblesLine % 2)) * 15.5 + 31 * j, 0, 0);
    
    	bubbleLine[rearBubblesLine].bubble[j].animation = "idle";
    	bubbleLine[rearBubblesLine].add(bubbleLine[rearBubblesLine].bubble[j]);
    
    
    
     
    
    }
    bubbleLine[rearBubblesLine].position.y = 27 * (rearBubblesLine - 100);
  
    bubblesContainer.add(bubbleLine[rearBubblesLine]);
    
    rearBubblesLine++;
    
    bubblesWaveCount++;
    
	}
	
	
	gsap.to(bubblesContainer.position, { duration: 0.3, y: bubblesContainer.position.y - 27, ease: "power1.inOut", onComplete: function() {
	  //alert(bubblesContainer.position.y + bubbleLine[frontBubblesLine].position.y)
	  
	  if (bubblesContainer.position.y + bubbleLine[frontBubblesLine].position.y > 115) {
      moveBubblesWave();
	  } else if (bubblesContainer.position.y + bubbleLine[frontBubblesLine].position.y < -46) {
		 	checkHandToHandAttack(false);
		} else {
		  isCellsFieldActive = true;
		}
	} });
	

	
	
	
	
}


function checkHandToHandAttack(goWave) {
	let closestLine = rearBubblesLine;
	for (let i = frontBubblesLine; i < rearBubblesLine; i++) {
    if (bubbleLine[i].position.y + bubblesContainer.position.y == 7) {
    	closestLine = i;
    	break;
    }
	}
	for (let i = frontBubblesLine + 1; i < closestLine; i++) {
		for (let j = 0; j < bubbleLine[i].bubble.length; j++) {
			if (bubbleLine[i].bubble[j].content >= 0) {
		    for (let k = 53; k >= 0; k--) {
		    	if (cell[k].content >= 0 && new THREE.Vector2(cell[k].position.x, cell[k].position.y).distanceTo(new THREE.Vector2(bubbleLine[i].bubble[j].position.x, bubbleLine[i].position.y + bubblesContainer.position.y)) < 65) {

		     // if (cell[k].content >= 0 && Math.abs(cell[k].position.y - bubblesContainer.position.y - bubbleLine[i].position.y) < 62 && Math.abs(cell[k].position.x - bubbleLine[i].bubble[j].position.x) < 62) {
		    	  if (cell[k].content == bubbleLine[i].bubble[j].content && cell[k].content < 6) {
		    	  	cellsTargets.push([k, i, j]);
		    	  } else {
		    	  	bubblesTargets.push([i, j, k]);
		    	  }
		      }
	    	}
			}
		}
	}
	if (cellsTargets.length > 0 || bubblesTargets.length > 0) {
		shuffle(cellsTargets);
		shuffle(bubblesTargets);
		goHandToHandCombat(goWave);
	} else {
		if (goWave) {
			moveBubblesWave();
		} else {
	  	isCellsFieldActive = true;
		}
	}
}

function goHandToHandCombat(goWave) {
	const killer = []
	const victim = []
	
	
	let cellsFinished = true;
	let bubblesFinished = true;
	for (let i = 0; i < cellsTargets.length; i++) {
		if (bubbleLine[cellsTargets[i][1]].bubble[cellsTargets[i][2]].content >= 0 && cell[cellsTargets[i][0]].animation == "idle") {
			cellsFinished = false;
			cell[cellsTargets[i][0]].animation = "combat"
			
			
			killer.push(cellsTargets[i][0])
			bubbleLine[cellsTargets[i][1]].bubble[cellsTargets[i][2]].content = -1;
			bubbleLine[cellsTargets[i][1]].bubble[cellsTargets[i][2]].body.scale.set(2, 2, 2);
      gsap.to(cell[cellsTargets[i][0]].scale, { duration: 0.2, x: 1.3, y: 1.3, ease: "power1.out", repeat: 1, yoyo: true });
      gsap.to(bubbleLine[cellsTargets[i][1]].bubble[cellsTargets[i][2]].pic.scale, { duration: 0.4, x: 0, y: 0, ease: "power1.out", onComplete: function() {
        cell[cellsTargets[i][0]].animation = "idle"
      } });
		}
	}
	for (let i = 0; i < bubblesTargets.length; i++) {
		if (bubbleLine[bubblesTargets[i][0]].bubble[bubblesTargets[i][1]].content >= 0 && cell[bubblesTargets[i][2]].animation == "idle" && bubbleLine[bubblesTargets[i][0]].bubble[bubblesTargets[i][1]].animation == "idle" && cell[bubblesTargets[i][2]].content >= 0) {
			bubblesFinished = false;
			
			
			bubbleLine[bubblesTargets[i][0]].bubble[bubblesTargets[i][1]].animation = "combat";

			//
			victim.push(bubblesTargets[i][2])
			gsap.to(cell[bubblesTargets[i][2]].scale, { duration: 0.4, x: 0, y: 0, ease: "power1.out", onComplete: function() {
				cell[bubblesTargets[i][2]].pic.visible = false;
				cell[bubblesTargets[i][2]].scale.set(1, 1, 1)
			} });

			if (cell[bubblesTargets[i][2]].content > 5) shootingBall[bubblesTargets[i][2]].visible = false;
			cell[bubblesTargets[i][2]].content = -2;
			cell[bubblesTargets[i][2]].movable = 0;
			//cell[bubblesTargets[i][2]].clickableArea.material = whiteBasicMaterial;
			
      gsap.to(bubbleLine[bubblesTargets[i][0]].bubble[bubblesTargets[i][1]].scale, { duration: 0.2, x: 1.3, y: 1.3, ease: "power1.out", repeat: 1, yoyo: true, onComplete: function() {
      	bubbleLine[bubblesTargets[i][0]].bubble[bubblesTargets[i][1]].animation = "idle";

      } });

		}
	}
//	alert(killer)
//	alert(victim)
	

	if (cellsFinished && bubblesFinished) {
		
		
		
		checkEdgeLines();
		cellsTargets = [];
		bubblesTargets = [];
		//alert(bubblesTargets)
		for (let i = 53; i >= 0; i--) {
			
		  if (cell[i].content < 0) {
			  cell[i].content = -1;
			  cell[i].movable = 1;
			 // cell[i].clickableArea.material = transparentBasicMaterial;

			  for (let j = frontBubblesLine + 1; j < rearBubblesLine; j++) {
		  		for (let k = 0; k < bubbleLine[j].bubble.length; k++) {
		  			
		  		//	if (cell[k].content >= 0 && new THREE.Vector2(cell[k].position.x, cell[k].position.y).distanceTo(new THREE.Vector2(bubbleLine[i].bubble[j].position.x, bubbleLine[i].bubble[j].position.y + bubblesContainer.position.y)) < 65) {
            if (bubbleLine[j].bubble[k].content >= 0 && new THREE.Vector2(cell[i].position.x, cell[i].position.y).distanceTo(new THREE.Vector2(bubbleLine[j].bubble[k].position.x, bubbleLine[j].position.y + bubblesContainer.position.y)) < 65) {

				  	//if (bubbleLine[j].bubble[k].content >= 0 && Math.abs(cell[i].position.y - bubblesContainer.position.y - bubbleLine[j].position.y) < 62 && Math.abs(cell[i].position.x - bubbleLine[j].bubble[k].position.x) < 62) {
					  	cell[i].content = -2;
					   	cell[i].movable = 0;
					   //	cell[i].clickableArea.material = whiteBasicMaterial;

					  	//alert(i)
					  	break;
					  }
			  	}
			  }
		  }
		  //if (cell[i].content == -2) alert(i)
		}
		if (goWave) {
			moveBubblesWave();
		} else {
			autoWave = false;
		  goFillEmptyCells();
		
		}
		
		//isCellsFieldActive = true;
	} else {
		setTimeout(function() {
			requestAnimationFrame(function() {
		    goHandToHandCombat(goWave);
			});
		}, 400);
		
	}
}


function shuffle(array) {
  let currentIndex = array.length,  randomIndex;
  while (currentIndex != 0) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;
    [array[currentIndex], array[randomIndex]] = [array[randomIndex], array[currentIndex]];
  }
}


function checkMatches(cellsToCheck, fullCheck, playerMove) {
	let matchedCells = [];
	let tempMatches = [];
	let additionalTempMatches = [];
	let startCell;
	let endCell;
	if (fullCheck) {
  	for (let i = 0; i < cellsToCheck.length; i++) {
		  if (cell[cellsToCheck[i]].content >= 0 && cell[cellsToCheck[i]].tempMark == 0) {
		    if (cellsToCheck[i] % 9 > 1 && cellsToCheck[i] % 9 < 7) {
			    for (let j = cellsToCheck[i] - 2; j <= cellsToCheck[i] + 2; j++) {
		  	  	if (cell[j].tempMark == 0 && cell[j].content == cell[cellsToCheck[i]].content) {
	  			  	tempMatches.push(j);
	  		  	} else {
			    		tempMatches = [];
			  	  	break;
		  		  }
  		  	}
		    }
			  if (tempMatches.length == 0 && cellsToCheck[i] - 18 >= 0 && cellsToCheck[i] + 18 <= 53) {
			  	for (let j = cellsToCheck[i] - 18; j <= cellsToCheck[i] + 18; j += 9) {
					  if (cell[j].tempMark == 0 && cell[j].content == cell[cellsToCheck[i]].content) {
					  	tempMatches.push(j);
				  	} else {
				  		tempMatches = [];
				  		break;
				  	}
			  	}
		  	}
		  	if (tempMatches.length > 0) {
			  	tempMatches.forEach(function(item) { cell[item].tempMark = 1 });
			  	matchedCells.push([4, tempMatches[2], tempMatches[0], tempMatches[1], tempMatches[3], tempMatches[4]]);
			  	tempMatches = [];
		  	}
	  	}
	  }
  	for (let i = 0; i < cellsToCheck.length; i++) {
		  if (cell[cellsToCheck[i]].content >= 0 && cell[cellsToCheck[i]].tempMark == 0) {
		  	startCell = cellsToCheck[i] % 9 > 1 ? cellsToCheck[i] - 2 : Math.floor(cellsToCheck[i] / 9) * 9;
		  	endCell = cellsToCheck[i] % 9 < 7 ? cellsToCheck[i] + 2 : Math.ceil(cellsToCheck[i] / 9) * 9 - 1;
		  	for (let j = startCell; j <= endCell; j++) {
		  		if (cell[j].tempMark == 0 && cell[j].content == cell[cellsToCheck[i]].content) {
			  		tempMatches.push(j);
			  		if (tempMatches.length == 3) break;
			  	} else {
				  	if (tempMatches.length < 3) {
					  	tempMatches = [];
				  	} else {
				  		break;
				  	}
			  	}
		  	}
		  	if (tempMatches.length == 3) {
			  	startCell = cellsToCheck[i] - 18 >= 0 ? cellsToCheck[i] - 18 : cellsToCheck[i] % 9;
		  		endCell = cellsToCheck[i] + 18 <= 53 ? cellsToCheck[i] + 18 : 45 + cellsToCheck[i] % 9;
		  		for (let j = startCell; j <= endCell; j += 9) {
				  	if (cell[j].tempMark == 0 && cell[j].content == cell[cellsToCheck[i]].content) {
					  	additionalTempMatches.push(j);
					  	if (additionalTempMatches.length == 3) break;
					  } else {
						  if (additionalTempMatches.length < 3) {
						  	additionalTempMatches = [];
					  	} else {
						  	break;
					  	}
					  }
			  	}
			  	if (additionalTempMatches.length < 3) {
					  tempMatches = [];
				  	additionalTempMatches = [];
				  	continue;
				  }
		  	} else {
			  	tempMatches = [];
		  		continue;
		  	}
		  	if (additionalTempMatches.length > 0) {
				  tempMatches.forEach(function(item) { cell[item].tempMark = 1 });
			  	additionalTempMatches.forEach(function(item) { cell[item].tempMark = 1 });
			    for (let j = 0; j < 3; j++) {
			    	if (tempMatches.find(item => item == additionalTempMatches[j]) !== undefined) {
			    	  matchedCells[matchedCells.length] = [3, additionalTempMatches[j]];
			    		tempMatches.splice(tempMatches.indexOf(additionalTempMatches[j]), 1);
			    		additionalTempMatches.splice(additionalTempMatches.indexOf(additionalTempMatches[j]), 1);
			    		matchedCells[matchedCells.length - 1] = matchedCells[matchedCells.length - 1].concat(tempMatches, additionalTempMatches);
              break;
			    	}
			    }
		      tempMatches = [];
		      additionalTempMatches = [];
		  	}
  		}
  	}
  	for (let i = 0; i < cellsToCheck.length; i++) {
		  if (cell[cellsToCheck[i]].content >= 0 && cell[cellsToCheck[i]].tempMark == 0) {
		  	startCell = cellsToCheck[i] % 9 > 1 ? cellsToCheck[i] - 2 : Math.floor(cellsToCheck[i] / 9) * 9;
			  endCell = cellsToCheck[i] % 9 < 7 ? cellsToCheck[i] + 2 : Math.ceil(cellsToCheck[i] / 9) * 9 - 1;
			  for (let j = startCell; j <= endCell; j++) {
				  if (cell[j].tempMark == 0 && cell[j].content == cell[cellsToCheck[i]].content) {
				  	tempMatches.push(j);
		   			if (tempMatches.length == 4) {
		   				break;
		   			}
			  	} else {
				  	if (tempMatches.length < 4) {
						  tempMatches = [];
				  	} else {
				  		break;
				  	}
			  	}
		  	}
			  if (tempMatches.length < 4) {
				  tempMatches = [];
			  	startCell = cellsToCheck[i] - 18 >= 0 ? cellsToCheck[i] - 18 : cellsToCheck[i] % 9;
			  	endCell = cellsToCheck[i] + 18 <= 53 ? cellsToCheck[i] + 18 : 45 + cellsToCheck[i] % 9;
				  for (let j = startCell; j <= endCell; j += 9) {
				  	if (cell[j].tempMark == 0 && cell[j].content == cell[cellsToCheck[i]].content) {
						  tempMatches.push(j);
						  if (tempMatches.length == 4) {
						  	break;
						  }
					  } else {
						  if (tempMatches.length < 4) {
						  	tempMatches = [];
					  	} else {
						  	break;
					  	}
				  	}
			  	}
			  	if (tempMatches.length < 4) {
			  		tempMatches = [];
			  	  continue;
			  	}
		  	}
			  if (tempMatches.length > 0) {
			  	tempMatches.forEach(function(item) { cell[item].tempMark = 1 });
			  	if (!playerMove) {
		  	  	matchedCells.push([2, tempMatches[1], tempMatches[0], tempMatches[2], tempMatches[3]]);
			  	} else {
			  		matchedCells.push([2, cellsToCheck[i]]);
			  		tempMatches.splice(tempMatches.indexOf(cellsToCheck[i]), 1);
            matchedCells[matchedCells.length - 1] = matchedCells[matchedCells.length - 1].concat(tempMatches);
			  	}
			  	tempMatches = [];
		  	}
		  }
    }
	}
  for (let i = 0; i < cellsToCheck.length; i++) {
  	if (cell[cellsToCheck[i]].content >= 0 && cell[cellsToCheck[i]].tempMark == 0) {
			if (cellsToCheck[i] % 9 > 0 && cell[cellsToCheck[i] - 1].tempMark == 0 && cell[cellsToCheck[i] - 1].content == cell[cellsToCheck[i]].content) {
				if (cellsToCheck[i] < 45 && cell[cellsToCheck[i] + 8].tempMark == 0 && cell[cellsToCheck[i] + 9].tempMark == 0 && cell[cellsToCheck[i] + 8].content == cell[cellsToCheck[i]].content && cell[cellsToCheck[i] + 9].content == cell[cellsToCheck[i]].content) {
					matchedCells.push([1, cellsToCheck[i], cellsToCheck[i] - 1, cellsToCheck[i] + 8, cellsToCheck[i] + 9]);
          for (let j = 1; j < 5; j++) {
          	cell[matchedCells[matchedCells.length - 1][j]].tempMark = 1;
          }
          continue;
				}
				if (cellsToCheck[i] > 8 && cell[cellsToCheck[i] - 9].tempMark == 0 && cell[cellsToCheck[i] - 10].tempMark == 0 && cell[cellsToCheck[i] - 9].content == cell[cellsToCheck[i]].content && cell[cellsToCheck[i] - 10].content == cell[cellsToCheck[i]].content) {
					matchedCells.push([1, cellsToCheck[i], cellsToCheck[i] - 1, cellsToCheck[i] - 9, cellsToCheck[i] - 10]);
					for (let j = 1; j < 5; j++) {
						cell[matchedCells[matchedCells.length - 1][j]].tempMark = 1;
					}
					continue;
				}
			}
			if (cellsToCheck[i] % 9 < 8 && cell[cellsToCheck[i] + 1].tempMark == 0 && cell[cellsToCheck[i] + 1].content == cell[cellsToCheck[i]].content) {
				if (cellsToCheck[i] < 45 && cell[cellsToCheck[i] + 9].tempMark == 0 && cell[cellsToCheck[i] + 10].tempMark == 0 && cell[cellsToCheck[i] + 9].content == cell[cellsToCheck[i]].content && cell[cellsToCheck[i] + 10].content == cell[cellsToCheck[i]].content) {
					matchedCells.push([1, cellsToCheck[i], cellsToCheck[i] + 1, cellsToCheck[i] + 9, cellsToCheck[i] + 10]);
					for (let j = 1; j < 5; j++) {
						cell[matchedCells[matchedCells.length - 1][j]].tempMark = 1;
					}
					continue;
				}
				if (cellsToCheck[i] > 8 && cell[cellsToCheck[i] - 8].tempMark == 0 && cell[cellsToCheck[i] - 9].tempMark == 0 && cell[cellsToCheck[i] - 8].content == cell[cellsToCheck[i]].content && cell[cellsToCheck[i] - 9].content == cell[cellsToCheck[i]].content) {
					matchedCells.push([1, cellsToCheck[i], cellsToCheck[i] + 1, cellsToCheck[i] - 8, cellsToCheck[i] - 9]);
					for (let j = 1; j < 5; j++) {
						cell[matchedCells[matchedCells.length - 1][j]].tempMark = 1;
					}
				}
			}
  	}
  }
  for (let i = 0; i < cellsToCheck.length; i++) {
  	if (cell[cellsToCheck[i]].content >= 0 && cell[cellsToCheck[i]].tempMark == 0) {
			startCell = cellsToCheck[i] % 9 > 1 ? cellsToCheck[i] - 2 : Math.floor(cellsToCheck[i] / 9) * 9;
			endCell = cellsToCheck[i] % 9 < 7 ? cellsToCheck[i] + 2 : Math.ceil(cellsToCheck[i] / 9) * 9 - 1;
			for (let j = startCell; j <= endCell; j++) {
				if (cell[j].tempMark == 0 && cell[j].content == cell[cellsToCheck[i]].content) {
					tempMatches.push(j);
					if (tempMatches.length == 3) break;
				} else {
					if (tempMatches.length < 3) {
						tempMatches = [];
					} else {
						break;
					}
				}
			}
			if (tempMatches.length < 3) {
				tempMatches = [];
				startCell = cellsToCheck[i] - 18 >= 0 ? cellsToCheck[i] - 18 : cellsToCheck[i] % 9;
				endCell = cellsToCheck[i] + 18 <= 53 ? cellsToCheck[i] + 18 : 45 + cellsToCheck[i] % 9;
				for (let j = startCell; j <= endCell; j += 9) {
					if (cell[j].tempMark == 0 && cell[j].content == cell[cellsToCheck[i]].content) {
						tempMatches.push(j);
						if (tempMatches.length == 3) break;
					} else {
						if (tempMatches.length < 3) {
							tempMatches = [];
						} else {
							break;
						}
					}
				}
				if (tempMatches.length < 3) {
					tempMatches = [];
				  continue;
				}
			}
			if (tempMatches.length > 0) {
				tempMatches.forEach(function(item) { cell[item].tempMark = 1 });
				if (!playerMove) {
				  matchedCells.push([0, tempMatches[1], tempMatches[0], tempMatches[2]]);
				} else {
					matchedCells.push([0, cellsToCheck[i]]);
			  	tempMatches.splice(tempMatches.indexOf(cellsToCheck[i]), 1);
          matchedCells[matchedCells.length - 1] = matchedCells[matchedCells.length - 1].concat(tempMatches);
				}
				tempMatches = [];
			}
		}
	}
	cell.forEach(function(item) { item.tempMark = 0 });
	if (matchedCells == 0) {
		matchedCells = false;
	}
	return matchedCells;
}

function goMergeCells(matchedCells) {
	for (let i = 0; i < matchedCells.length; i++) {
	  for (let j = 2; j < matchedCells[i].length; j++) {
		  const movedCell = cell[matchedCells[i][j]];
		  gsap.to(movedCell.pic.position, { duration: 0.25, x: cell[matchedCells[i][1]].position.x - cell[matchedCells[i][j]].position.x, y: cell[matchedCells[i][1]].position.y - cell[matchedCells[i][j]].position.y, ease: "back.in", delay: 0.25 * i, onComplete: function() {
		    movedCell.pic.visible = false;
		    movedCell.content = -1;
		  } });
  	}
  	
  	gsap.to(cell[matchedCells[i][0]].pic.position, { duration: 0.25, x: 0, y: 0, ease: "back.in", delay: 0.25 * i, onComplete: function() {
		  createShootingBall(matchedCells[i]);
		  if (i == matchedCells.length - 1) goFillEmptyCells();
		  
		} });
	}
}

function createShootingBall(matchedCells) {
	
	if (matchedCells[0] == 0) {
		shootingBall[matchedCells[1]] = new THREE.Mesh(new THREE.PlaneGeometry(34, 34), new THREE.MeshBasicMaterial({ map: tex[shootingBallPic[1][cell[matchedCells[1]].content]], transparent: true }));
		shootingBall[matchedCells[1]].content = cell[matchedCells[1]].content;
		shootingBall[matchedCells[1]].position.set(cell[matchedCells[1]].position.x, cell[matchedCells[1]].position.y, -1);
		gameContainer.add(shootingBall[matchedCells[1]]);
		cell[matchedCells[1]].pic.material.map = tex[4];
		cell[matchedCells[1]].content = 6;
		cell[matchedCells[1]].movable = 0;
	}
	if (matchedCells[0] == 1) {
		shootingBall[matchedCells[1]] = new THREE.Mesh(new THREE.PlaneGeometry(34, 34), new THREE.MeshBasicMaterial({ map: tex[shootingBallPic[1][cell[matchedCells[1]].content]], transparent: true }));
		shootingBall[matchedCells[1]].content = cell[matchedCells[1]].content;
		shootingBall[matchedCells[1]].position.set(cell[matchedCells[1]].position.x, cell[matchedCells[1]].position.y, -1);
		gameContainer.add(shootingBall[matchedCells[1]]);
		cell[matchedCells[1]].pic.material.map = tex[4];
		cell[matchedCells[1]].content = 6;
		cell[matchedCells[1]].movable = 0;
	}
	if (matchedCells[0] == 2) {
		shootingBall[matchedCells[1]] = new THREE.Mesh(new THREE.PlaneGeometry(34, 34), new THREE.MeshBasicMaterial({ map: tex[shootingBallPic[1][cell[matchedCells[1]].content]], transparent: true }));
		shootingBall[matchedCells[1]].content = cell[matchedCells[1]].content;
		shootingBall[matchedCells[1]].position.set(cell[matchedCells[1]].position.x, cell[matchedCells[1]].position.y, -1);
		gameContainer.add(shootingBall[matchedCells[1]]);
		cell[matchedCells[1]].pic.material.map = tex[4];
		cell[matchedCells[1]].content = 6;
		cell[matchedCells[1]].movable = 0;
	}
	if (matchedCells[0] == 3) {
		shootingBall[matchedCells[1]] = new THREE.Mesh(new THREE.PlaneGeometry(34, 34), new THREE.MeshBasicMaterial({ map: tex[shootingBallPic[1][cell[matchedCells[1]].content]], transparent: true }));
		shootingBall[matchedCells[1]].content = cell[matchedCells[1]].content;
		shootingBall[matchedCells[1]].position.set(cell[matchedCells[1]].position.x, cell[matchedCells[1]].position.y, -1);
		gameContainer.add(shootingBall[matchedCells[1]]);
		cell[matchedCells[1]].pic.material.map = tex[4];
		cell[matchedCells[1]].content = 6;
		cell[matchedCells[1]].movable = 0;
	}
	if (matchedCells[0] == 4) {
		shootingBall[matchedCells[1]] = new THREE.Mesh(new THREE.PlaneGeometry(34, 34), new THREE.MeshBasicMaterial({ map: tex[shootingBallPic[1][cell[matchedCells[1]].content]], transparent: true }));
		shootingBall[matchedCells[1]].content = cell[matchedCells[1]].content;
		shootingBall[matchedCells[1]].position.set(cell[matchedCells[1]].position.x, cell[matchedCells[1]].position.y, -1);
		gameContainer.add(shootingBall[matchedCells[1]]);
		cell[matchedCells[1]].pic.material.map = tex[4];
		cell[matchedCells[1]].content = 6;
		cell[matchedCells[1]].movable = 0;
	}
}




function goFillEmptyCells(nextMoveBubbles) {
	let refillArray = [];	
	for (let i = 53; i >= 0; i--) {
		
		
    if (cell[i].content == -1 && !cell[i].pathData) {
    	//cell[i].clickableArea.material = transparentBasicMaterial;

    	cell[i].traceData = 0;
    	let currentStep = 0;
    	let isTracing = true;
    	while (isTracing) {
    		isTracing = false;
    		for (let j = 53; j >= 0; j--) {
    			if (cell[j].traceData == currentStep) {
    				for (let k = 53; k >= 0; k--) {
    					if (k != j && cell[k].content > -2 && cell[k].content < 6 && cell[k].traceData == -1 && !cell[j].pathData && Math.abs(cell[j].position.x - cell[k].position.x) + Math.abs(cell[j].position.y - cell[k].position.y) == 38) {
    						cell[k].traceData = currentStep + 1;
    						isTracing = true;
    					}
    				}
    			}
    		}
    		currentStep++;
    	}
    	for (let j = 0; j < 9; j++) {
    		if (cell[j].traceData >= 0 && cell[j].traceData < currentStep) {
    			currentStep = cell[j].traceData;
    			isTracing = true;
    		}
    	}
    	if (isTracing) {
    		let currentCell = 0;
    		for (let j = 0; j < 9; j++) {
    			if (cell[j].traceData == currentStep) {
    				currentCell = j;
    				cell[currentCell].pathData = [0, -38, Math.floor(Math.random() * 3)];
    				refillArray.push(j);
    				break;
    			}
    		}
    		while (currentStep > 0) {
    			let startCell = currentCell < 45 ? currentCell + 9 : currentCell + 1;
    			if (startCell > 53) startCell = 53;
    			for (let j = startCell; j >= 0; j--) {
    				if (cell[j].traceData == currentStep - 1 && Math.abs(cell[j].position.x - cell[currentCell].position.x) + Math.abs(cell[j].position.y - cell[currentCell].position.y) == 38) {
    					cell[j].pathData = [cell[currentCell].position.x - cell[j].position.x, cell[currentCell].position.y - cell[j].position.y, cell[currentCell].content];
    					currentCell = j;
    					refillArray.push(j);
    					break;
    				}
    			}
    			currentStep--;
    		}
    	}
      cell.forEach(function(item) { item.traceData = -1 });
    }
	}
	if (refillArray.length > 0) {
  	for (let i = 0; i < refillArray.length; i++) {
		  if (cell[refillArray[i]].pathData) {
		  	cell[refillArray[i]].pic.position.set(cell[refillArray[i]].pathData[0], cell[refillArray[i]].pathData[1], 0);
		  	cell[refillArray[i]].content = cell[refillArray[i]].pathData[2];
  			cell[refillArray[i]].pic.material.map = tex[cell[refillArray[i]].pathData[2]];
		  	if (cell[refillArray[i]].pathData[2] >= 0) {
		  		cell[refillArray[i]].pic.visible = true;
		  		cell[refillArray[i]].movable = 1;
		  	} else {
		  		cell[refillArray[i]].pic.visible = false;
		  	}
		  	cell[refillArray[i]].pathData = false;
		  	gsap.to(cell[refillArray[i]].pic.position, { duration: 0.2, x: 0, y: 0, ease: "none" });
		  }
		  if (newCells.find(item => item == refillArray[i]) === undefined) newCells.push(refillArray[i]);
  	}
	  setTimeout(function() {
	  	requestAnimationFrame(function() {
	  		goFillEmptyCells();
	  	})
	  }, 200);
	} else {
		const matchedCells = checkMatches(newCells, true, false);
  	if (matchedCells) {
  	  goMergeCells(matchedCells);
  	} else {
  		if (autoWave) {
  	  	moveBubblesWave();
  		} else {
  			isCellsFieldActive = true;
  			autoWave = true;
  		}
  		
  	}
		newCells = [];
	}
}








function goMoveCell(movedCells) {
	gsap.to(cell[movedCells[0]].pic.position, { duration: 0.15, x: cell[movedCells[1]].position.x - cell[movedCells[0]].position.x, y: cell[movedCells[1]].position.y - cell[movedCells[0]].position.y, ease: "power1.inOut" });
  gsap.to(cell[movedCells[1]].pic.position, { duration: 0.15, x: cell[movedCells[0]].position.x - cell[movedCells[1]].position.x, y: cell[movedCells[0]].position.y - cell[movedCells[1]].position.y, ease: "power1.inOut", onComplete: function() {
    const tempContent = cell[movedCells[0]].content;
    cell[movedCells[0]].pic.position.set(0, 0, 0);
    cell[movedCells[1]].pic.position.set(0, 0, 0);
    cell[movedCells[0]].content = cell[movedCells[1]].content;
    cell[movedCells[0]].pic.material.map = tex[cell[movedCells[0]].content];
    cell[movedCells[1]].content = tempContent;
  	cell[movedCells[1]].pic.material.map = tex[cell[movedCells[1]].content];
  	if (cell[movedCells[1]].content == -1) {
  	  cell[movedCells[0]].pic.visible = true;
  	  cell[movedCells[1]].pic.visible = false;
  	}
  	const matchedCells = checkMatches(movedCells, true, true);
  	if (matchedCells) {
    	goMergeCells(matchedCells);
  	} else {
  	  if (cell[movedCells[0]].content == cell[movedCells[1]].content) {
  	  	moveBubblesWave();
  	  } else {
    	  gsap.to(cell[movedCells[0]].pic.position, { duration: 0.15, x: cell[movedCells[1]].position.x - cell[movedCells[0]].position.x, y: cell[movedCells[1]].position.y - cell[movedCells[0]].position.y, ease: "power1.inOut" });
        gsap.to(cell[movedCells[1]].pic.position, { duration: 0.15, x: cell[movedCells[0]].position.x - cell[movedCells[1]].position.x, y: cell[movedCells[0]].position.y - cell[movedCells[1]].position.y, ease: "power1.inOut", onComplete: function() {
   	      const tempContent = cell[movedCells[0]].content;
          cell[movedCells[0]].pic.position.set(0, 0, 0);
          cell[movedCells[1]].pic.position.set(0, 0, 0);
    	    cell[movedCells[0]].content = cell[movedCells[1]].content;
          cell[movedCells[0]].pic.material.map = tex[cell[movedCells[0]].content];
          cell[movedCells[1]].content = tempContent;
          cell[movedCells[1]].pic.material.map = tex[cell[movedCells[1]].content];
          if (cell[movedCells[0]].content == -1) {
          	cell[movedCells[0]].pic.visible = false;
          	cell[movedCells[1]].pic.visible = true;
          }
          isCellsFieldActive = true;
          
          
          
        } });
  	  }
    }
  } });
}




function activateTower() {
	
		
	
	isCellsFieldActive = false;
	shootingTowerUI.position.set(cell[activeTower].position.x, cell[activeTower].position.y, 0);
		
  shootingTowerUI.visible = true;
  shootingTowerUI.outerCircle.material.map = tex[shootingTowerPic[2][shootingBall[activeTower].content]];
   
        
  shootingTowerUI.aimLine.material.map = tex[shootingTowerPic[3][shootingBall[activeTower].content]];
    
  shootingTowerUI.aim.material.map = tex[shootingTowerPic[4][shootingBall[activeTower].content]];

	shootingTowerUI.outerCircle.tween = gsap.to(shootingTowerUI.outerCircle.scale, { duration: 0.2, x: 1, y: 1, ease: "back.out" });

	
	
	
//  deactivateTower();
}

function deactivateTower() {
	activeTower = -1;
	if (shootingTowerUI.outerCircle.tween !== undefined && shootingTowerUI.outerCircle.tween !== null) {
		shootingTowerUI.outerCircle.tween.kill();
		shootingTowerUI.outerCircle.tween = null;
	}
	
	
	gsap.to(shootingTowerUI.outerCircle.scale, { duration: 0.1, x: 0, y: 0, ease: "back.in", onComplete: function() {
		shootingTowerUI.visible = false;
		isCellsFieldActive = true;
	} });
  
	

}




function goAim() {
	shootingTowerUI.aim.visible = false;
	targetBubbles = [];
	shotPath = [];
	let emptyBubbles = [];
	let intersectPoint = false;
	let enemyDistance = Infinity;
  aimRay.set(new THREE.Vector3(cell[activeTower].position.x * mainScene.scale.x, (cell[activeTower].position.y + 13) * mainScene.scale.x, 0), new THREE.Vector3(document.body.clientWidth / mainScene.scale.x / 2 * mouse.x - cell[activeTower].position.x, document.body.clientHeight / mainScene.scale.x / 2 * mouse.y - (cell[activeTower].position.y + 13), 0).normalize(), 0, 560);
  for (let i = frontBubblesLine; i < rearBubblesLine; i++) {
	  for (let j = 0; j < bubbleLine[i].bubble.length; j++) {
	    if (aimRay.intersectObject(bubbleLine[i].bubble[j]).length > 0) {
        if (bubbleLine[i].bubble[j].content >= 0) {
        	if (aimRay.intersectObject(bubbleLine[i].bubble[j])[0].distance < enemyDistance) {
            enemyDistance = aimRay.intersectObject(bubbleLine[i].bubble[j])[0].distance;
            intersectPoint = new THREE.Vector2(aimRay.intersectObject(bubbleLine[i].bubble[j])[0].point.x / mainScene.scale.x, aimRay.intersectObject(bubbleLine[i].bubble[j])[0].point.y / mainScene.scale.x - bubblesContainer.position.y - 13);
        	}
        } else {
        	emptyBubbles.push([i, j]);
        }
	    }
	  }
  }
  if (intersectPoint) {
  	let aimDistance = Infinity;
  	for (let i = 0; i < emptyBubbles.length; i++) {
  		if (new THREE.Vector2(bubbleLine[emptyBubbles[i][0]].bubble[emptyBubbles[i][1]].position.x, bubbleLine[emptyBubbles[i][0]].position.y).distanceTo(intersectPoint) <= aimDistance) {
  			aimDistance = new THREE.Vector2(bubbleLine[emptyBubbles[i][0]].bubble[emptyBubbles[i][1]].position.x, bubbleLine[emptyBubbles[i][0]].position.y).distanceTo(intersectPoint);
  			targetBubbles[0] = [emptyBubbles[i][0], emptyBubbles[i][1]];
  		}
  	}
  	shootingTowerUI.aim.position.set(bubbleLine[targetBubbles[0][0]].bubble[targetBubbles[0][1]].position.x, bubbleLine[targetBubbles[0][0]].position.y, 1);
  	shootingTowerUI.aim.visible = true;
  	shootingTowerUI.aimLine.scale.x = aimRay.intersectObject(bubbleLine[targetBubbles[0][0]].bubble[targetBubbles[0][1]])[0].distance / mainScene.scale.x / 630;
  	shotPath = [bubbleLine[targetBubbles[0][0]].bubble[targetBubbles[0][1]].position.x, bubbleLine[targetBubbles[0][0]].position.y + bubblesContainer.position.y, new THREE.Vector2(cell[activeTower].position.x, cell[activeTower].position.y).distanceTo(new THREE.Vector2(bubbleLine[targetBubbles[0][0]].bubble[targetBubbles[0][1]].position.x, bubbleLine[targetBubbles[0][0]].position.y + bubblesContainer.position.y))];
  } else {
  	shootingTowerUI.aimLine.scale.x = 1;
  	shotPath = [630 * Math.cos(shootingTowerUI.aimLine.rotation.z) + cell[activeTower].position.x, 630 * Math.sin(shootingTowerUI.aimLine.rotation.z) + cell[activeTower].position.y, 630];
  }
}



function goShoot() {

	shootingTowerUI.aimLine.visible = false;
	shootingTowerUI.aim.visible = false;
	
	
	
  gsap.to(shootingBall[activeTower].position, { duration: 0.0005 * shotPath[2], x: shotPath[0], y: shotPath[1], ease: "none", onComplete: function() {
  	
  	checkShoot();
  } });
  
	cell[activeTower].movable = 1;
	cell[activeTower].content = -1;
	cell[activeTower].pic.visible = false;
	
	if (shootingTowerUI.outerCircle.tween !== undefined && shootingTowerUI.outerCircle.tween !== null) {
		shootingTowerUI.outerCircle.tween.kill();
		shootingTowerUI.outerCircle.tween = null;
	}
	gsap.to(shootingTowerUI.outerCircle.scale, { duration: 0.1, x: 0, y: 0, ease: "back.in", onComplete: function() {
    shootingTowerUI.visible = false;
	
  } });
  
}

function checkShoot() {
	if (targetBubbles.length > 0) {
		let mathedBubbles = [[targetBubbles[0][0], targetBubbles[0][1]]];
		for (let i = 0; i < mathedBubbles.length; i++) {
			for (let j = mathedBubbles[i][0] - 1; j < mathedBubbles[i][0] + 2; j++) {
    	  if (j > frontBubblesLine && j < rearBubblesLine) {
          for (let k = mathedBubbles[i][1] - 1; k < mathedBubbles[i][1] + 2; k++) {
          	if (k >= 0 && k < bubbleLine[j].bubble.length && bubbleLine[j].bubble[k].content == shootingBall[activeTower].content && Math.abs(bubbleLine[mathedBubbles[i][0]].bubble[mathedBubbles[i][1]].position.x - bubbleLine[j].bubble[k].position.x) <= 31) {
              let newMatch = true;
              for (let l = 0; l < mathedBubbles.length; l++) {
              	if (mathedBubbles[l][0] == j && mathedBubbles[l][1] == k) {
              		newMatch = false;
              	}
              }
              if (newMatch) mathedBubbles.push([j, k]);
          	}
          }
    	  }
			}
		}
		if (mathedBubbles.length > 1) {
			for (let i = 1; i < mathedBubbles.length; i++) {
				bubbleLine[mathedBubbles[i][0]].bubble[mathedBubbles[i][1]].content = -1;
		   	bubbleLine[mathedBubbles[i][0]].bubble[mathedBubbles[i][1]].body.scale.set(2, 2, 2);

				gsap.to(bubbleLine[mathedBubbles[i][0]].bubble[mathedBubbles[i][1]].pic.scale, { duration: 0.2, x: 0, y: 0, ease: "back.in" });
			}
			gsap.to(shootingBall[activeTower].scale, { duration: 0.2, x: 0, y: 0, ease: "back.in", onComplete: function() {
        
        disposeObject(shootingBall[activeTower]);
        activeTower = -1;
        checkEdgeLines();
        goFillEmptyCells();
      } });
		} else {
			gsap.to(shootingBall[activeTower].scale, { duration: 0.2, x: 0, y: 0, ease: "back.in", onComplete: function() {
				bubbleLine[targetBubbles[0][0]].bubble[targetBubbles[0][1]].content = shootingBall[activeTower].content;

		    bubbleLine[targetBubbles[0][0]].bubble[targetBubbles[0][1]].pic.material = new THREE.MeshBasicMaterial({ map: tex[6 + bubbleLine[targetBubbles[0][0]].bubble[targetBubbles[0][1]].content], transparent: true })
					
        bubbleLine[targetBubbles[0][0]].bubble[targetBubbles[0][1]].body.scale.set(1, 1, 1);
        bubbleLine[targetBubbles[0][0]].bubble[targetBubbles[0][1]].pic.visible = true;
        gsap.to(bubbleLine[targetBubbles[0][0]].bubble[targetBubbles[0][1]].pic.scale, { duration: 0.2, x: 1, y: 1, ease: "back.out", onComplete: function() {
        	disposeObject(shootingBall[activeTower]);
        	activeTower = -1;
        	checkEdgeLines();
        	
        	
        	
        	
        	
        	if (bubblesContainer.position.y + bubbleLine[frontBubblesLine].position.y < -46) {
        		checkHandToHandAttack(true);
        	} else {
          	goFillEmptyCells();
        	}
        	
        	
        	
        } });
      } });
		}
		
	} else {
		gsap.to(shootingBall[activeTower].scale, { duration: 0.2, x: 0, y: 0, ease: "back.in", onComplete: function() {
      disposeObject(shootingBall[activeTower]);
      activeTower = -1;
      checkEdgeLines();
      goFillEmptyCells();
    } });
	}
	
}

function checkEdgeLines() {
	let lineEmpty = true;
	for (let i = 0; i < bubbleLine[frontBubblesLine].bubble.length; i++) {
		if (bubbleLine[frontBubblesLine].bubble[i].content >= 0) {
			lineEmpty = false;
			break;
		}
	}
	if (lineEmpty) {
		removeFrontLine();
	} else {
		frontBubblesLine--;
		if (bubbleLine[frontBubblesLine] === undefined) {
			createEmptyLine(frontBubblesLine);
			bubbleLine[frontBubblesLine].position.y = 27 * (frontBubblesLine - 100);
		}
	}
	function removeFrontLine() {
	  lineEmpty = true;
		for (let i = 0; i < bubbleLine[frontBubblesLine + 1].bubble.length; i++) {
			if (bubbleLine[frontBubblesLine + 1].bubble[i].content >= 0) {
				lineEmpty = false;
				break;
			}
		}
		if (lineEmpty) {
			frontBubblesLine++;
			removeFrontLine();
		}
	}
	if (bubblesWaveCount == campaignLevelData[currentLevel].bubblesMatrix.length) {
  	lineEmpty = true;
	  for (let i = 0; i < bubbleLine[rearBubblesLine - 1].bubble.length; i++) {
  		if (bubbleLine[rearBubblesLine - 1].bubble[i].content >= 0) {
			  lineEmpty = false;
		  	break;
	  	}
  	}
  	if (lineEmpty) {
		  removeRearLine();
  	} else {
		  rearBubblesLine++;
		  if (bubbleLine[rearBubblesLine - 1] === undefined) {
		  	createEmptyLine(rearBubblesLine - 1);
		  	bubbleLine[rearBubblesLine - 1].position.y = bubbleLine[rearBubblesLine - 2].position.y + 27;
  		}
  	}
  	function removeRearLine() {
	    lineEmpty = true;
		  for (let i = 0; i < bubbleLine[rearBubblesLine - 2].bubble.length; i++) {
			  if (bubbleLine[rearBubblesLine - 2].bubble[i].content >= 0) {
				  lineEmpty = false;
				  break;
			  }
  		}
		  if (lineEmpty) {
			  rearBubblesLine--;
		  	removeRearLine();
		  }
  	}
	}
	function createEmptyLine(line) {
		bubbleLine[line] = new THREE.Object3D();
		bubbleLine[line].bubble = [];
		for (let i = 0; i < 11 - (line % 2); i++) {
			bubbleLine[line].bubble[i] = new THREE.Object3D();
			bubbleLine[line].bubble[i].pic = new THREE.Mesh(new THREE.PlaneGeometry(38, 36), transparentBasicMaterial);
			bubbleLine[line].bubble[i].pic.geometry.translate(0, 1, 0);
			bubbleLine[line].bubble[i].body = new THREE.Mesh(new THREE.CylinderGeometry(20, 20, 200, 8, 1), transparentBasicMaterial);
			bubbleLine[line].bubble[i].body.rotation.x = Math.PI * 0.5;
			bubbleLine[line].bubble[i].body.visible = false;
			bubbleLine[line].bubble[i].pic.visible = false;
			bubbleLine[line].bubble[i].content = -1;
			bubbleLine[line].bubble[i].pic.scale.set(0, 0, 1);
			bubbleLine[line].bubble[i].body.scale.set(2, 2, 2);
			bubbleLine[line].bubble[i].add(bubbleLine[line].bubble[i].pic, bubbleLine[line].bubble[i].body);
			bubbleLine[line].bubble[i].position.set(-(10 - (line % 2)) * 15.5 + 31 * i, 0, 0);
			bubbleLine[line].bubble[i].animation = "idle";
			bubbleLine[line].add(bubbleLine[line].bubble[i]);
		}
		bubblesContainer.add(bubbleLine[line]);
	}
//	checkHandToHandAttack();
	
	
//	alert(frontBubblesLine)
}







function disposeObject(object) {
	object.parent.remove(object);
	const list = [];
	object.traverse((child) => {
		if (child.isMesh) list.push(child);
	});
	for (let i = 0; i < list.length; i++) {
		list[i].geometry.dispose();
		if (list[i].material.map) list[i].material.map.dispose();
		list[i].material.dispose();
	}
	object = null;
}




let onTouch = true;
const raycaster = new THREE.Raycaster(), mouse = new THREE.Vector2();

function onDocumentTouchStart(event) {
  event.preventDefault();
  if (event.changedTouches[0].identifier == 0) {
    mouse.x = (event.changedTouches[0].clientX / window.innerWidth) * 2 - 1;
    mouse.y = -(event.changedTouches[0].clientY / window.innerHeight) * 2 + 1;
  }
  raycaster.setFromCamera(mouse, mainCamera);
  if (event.touches.length == 1) checkUserAction("point", raycaster);
}
function onDocumentTouchEnd(event) {
	
	if (event.touches.length == 0) checkUserAction("release");
}
function onDocumentTouchMove(event) {
	event.preventDefault();
	if (event.changedTouches[0].identifier == 0) {
    mouse.x = (event.changedTouches[0].clientX / window.innerWidth) * 2 - 1;
    mouse.y = -(event.changedTouches[0].clientY / window.innerHeight) * 2 + 1;
	}
  raycaster.setFromCamera(mouse, mainCamera);
  
  checkUserAction("move", raycaster);
}


function checkUserAction(event, raycaster) {
	if (event == "point") {
  	if (isCellsFieldActive) {
    	for (let i = 0; i < 54; i++) {
	      if (raycaster.intersectObject(cell[i].clickableArea).length > 0) {
	  	    if (cell[i].movable == 1 && cell[i].content >= 0) {
		        cellsToCheck.push(i);
		        if (i % 9 > 0 && cell[i - 1].movable > 0) cellsToCheck.push(i - 1);
		        if (i % 9 < 8 && cell[i + 1].movable > 0) cellsToCheck.push(i + 1);
		        if (i < 45 && cell[i + 9].movable > 0) cellsToCheck.push(i + 9);
		        if (i > 8 && cell[i - 9].movable > 0) cellsToCheck.push(i - 9);
		        if (cellsToCheck.length > 1) {
		        	isCellsFieldActive = false;
		        } else {
		        	cellsToCheck = [];
		        }
	  	    }
	  	    if (cell[i].content > 5) {
	  	    	isCellsFieldActive = false;
	  	    	activeTower = i;
	  	    	activateTower();
	  	    }
	    	  break;
		    }
      }
    }
	} else if (event == "move") {
  	if (cellsToCheck.length > 0) {
  	  for (let i = 1; i < cellsToCheck.length; i++) {
    		if (raycaster.intersectObject(cell[cellsToCheck[i]].clickableArea).length > 0) {
  	  	  goMoveCell([cellsToCheck[i], cellsToCheck[0]]);
  	  	  cellsToCheck = [];
  	  	  break;
  	  	}
    	}
    }
    if (activeTower >= 0) {
    	if (raycaster.intersectObject(cell[activeTower].clickableArea).length > 0) {
    		if (shootingTowerUI.aimLine.visible) {
    			shootingTowerUI.aim.visible = false;
    			shootingTowerUI.aimLine.visible = false;
    			
    			if (shootingTowerUI.outerCircle.tween !== undefined && shootingTowerUI.outerCircle.tween !== null) {
    				shootingTowerUI.outerCircle.tween.kill();
    				shootingTowerUI.outerCircle.tween = null;
    			}
    			shootingTowerUI.outerCircle.tween = gsap.to(shootingTowerUI.outerCircle.scale, { duration: 0.2, x: 1, y: 1, ease: "back.out" });
    		}
    	} else {
    		shootingTowerUI.aimLine.rotation.z = Math.atan2((document.body.clientHeight / mainScene.scale.x / 2 * mouse.y - cell[activeTower].position.y - 13), (document.body.clientWidth / mainScene.scale.x / 2 * mouse.x - cell[activeTower].position.x));
    		
    		if (!shootingTowerUI.aimLine.visible) {
    			shootingTowerUI.aimLine.visible = true;
    			if (shootingTowerUI.outerCircle.tween !== undefined && shootingTowerUI.outerCircle.tween !== null) {
         		shootingTowerUI.outerCircle.tween.kill();
	        	shootingTowerUI.outerCircle.tween = null;
        	}
        	shootingTowerUI.outerCircle.tween = gsap.to(shootingTowerUI.outerCircle.scale, { duration: 0.1, x: 0, y: 0, ease: "back.in" });
    		}
    		goAim();
    	}
    }
	} else if (event == "release") {
		 
			
	  if (cellsToCheck.length > 0) {
		  cellsToCheck = [];
		 	isCellsFieldActive = true;
	  }
  	if (activeTower >= 0) {
  		
  		if (!shootingTowerUI.aimLine.visible) {
  		  deactivateTower();
  		} else {
  			goShoot();
  		}
  	}
	}
}





const stats = new Stats();
document.body.appendChild(stats.dom);


loop();
function loop() {
  if (oldWindow !== document.body.clientWidth / document.body.clientHeight) {
    onWindowResize();
  }
  stats.update();
  mainRenderer.render(mainScene, mainCamera);
  requestAnimationFrame(loop);
}